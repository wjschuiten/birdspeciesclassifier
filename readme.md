CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation and use
 * Configuration
 * Troubleshooting

INTRODUCTION
------------

The bird classifier can be used to classify which ecological species a bird belongs to, depending on it's bone size. 
Logistic regression alghoritm from WEKA 3.8 is used to classify the species of the bird.
It features a java module that can be built into a .fatjar for commandline use.

For more information, visit the main page:
bioinf.nl/~wjschuiten (dummy)

To see the original project data that the application is designed on, visit:
https://www.kaggle.com/zhangjuefei/birds-bones-and-living-habits

REQUIREMENTS
------------

*Java version 8 or higher + JDK
*WEKA version 3.8

INSTALLATION AND USE
------------------------

Use the source code to build a .fatjar as you normally would, other builds may provide issues.

The program is command line based and takes three arguments

-f <input file> /// --input_file <input file>
*the input file containing unknown, unclassified birds containing bone measurement data.

-o <output file> /// --output_file <output file> (optional)
*Output of the program will be written to the specified output file, if none is given, output is printed on the commandline instead

-h /// --help
*Prints help to the user

-m <model file> // --model_file <model file>
This is the model file on which the machine learning alghoritm is based, the one used for the current research is supplied in the model directory and it is recommended you use this as a default. Currently only logistic regression models are supported.

For testing how the program should run, a test file has been added in the ExampleTestFile folder. This can be given to the program as input file argument.

Note that the alghoritm uses bone length and width to determine the species. Measurements are needed from the humerus, ulnua, femur, tarsometatarsus and tibiotarsus.
Supply the data in an .arff file, other filetypes are not accepted. See WEKA 3.8 to convert common file types such as .csv to .arff.
label each column as the first 3 letters of the bone + l (length) or w (width). Your labels should be:
huml, humw, ulnl, ulnw, feml, femw, tarl, tarw, tibl, tibw.
Create an 11th column called type that only contains ? for each row which will be used for classification.
Refer to the original dataset for an example of a proper data frame.

CONFIGURATION
-------------

No configuration changes can be made to the program. If you wish to improve the alghoritm, use the original data set + WEKA 3.8 explorer to optimize.

TROUBLESHOOTING
---------------

If you are recieving file errors:
Both output and input file must be a .arff files or the program will exit. Your model file must end with .model and contain a proper logistic regression model.

If you are recieving no classifications of your file (every type is still ?):
Ensure the original type labels are added to the .arff file (SW, SO, W, P, T,R)


