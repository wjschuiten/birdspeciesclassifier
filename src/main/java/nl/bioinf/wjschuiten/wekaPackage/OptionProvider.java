package nl.bioinf.wjschuiten.wekaPackage;

public interface OptionProvider {
    /**
     * @return input file
     */
    String getInputFileName();

    /**
     * @return output file
     */
    String getOutputFileName();

    /**
     *
     * @return model file
     */
    String getModelFileName();

}
