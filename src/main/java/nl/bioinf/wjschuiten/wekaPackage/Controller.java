package nl.bioinf.wjschuiten.wekaPackage;

public class Controller{
    /**
     * get out of static
     * @param args
     */
        public static void main(final String[] args) {
            Controller c = new Controller();
            c.start(args);
        }

        public void start(String[] args){
            try {
                ApacheClientOptions op = new ApacheClientOptions(args);
                /**
                 * If user requests help, prints it then shuts down the program
                 */
                if (op.helpRequested()) {
                    op.printHelp();
                    return;
                }
                ApacheClientOptions.validateFileName(op.getInputFileName());
                /**
                 * If no outputfile is specified, call WekaRunner
                 */
                if (op.getOutputFileName() == null) {
                    new WekaRunner(op.getInputFileName(), op.getModelFileName());}
                else{
                    /**
                     * If outputfile is specified, overload WekaRunner with outputfile arg
                     */
                    ApacheClientOptions.validateFileName(op.getOutputFileName());
                    WekaRunner weka = new WekaRunner(op.getInputFileName(), op.getOutputFileName(), op.getModelFileName());
                }

                /**
                 * Prints exception message, then quits
                 */
            } catch (IllegalArgumentException ex) {
                System.err.println(ex.getMessage());
                return;
            }

        }
}