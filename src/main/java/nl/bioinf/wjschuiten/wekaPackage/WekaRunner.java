package nl.bioinf.wjschuiten.wekaPackage;

import weka.classifiers.functions.Logistic;
import weka.core.converters.ConverterUtils.DataSink;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class WekaRunner {
    protected String unknownFile;
    protected String outputfile;
    protected String modelFile;
    /**
     *Constructor when no output file is specified
     * @param inputFile
     */
    public WekaRunner(final String inputFile, String modelFile){
        this.unknownFile = inputFile;
        this.modelFile = modelFile;
        start();
    }

    /**
     * Constructor when output file is specified
     * @param inputFile
     * @param outputFile
     */
    public WekaRunner(final String inputFile, final String outputFile, final String modelFile){
        this.unknownFile = inputFile;
        this.outputfile = outputFile;
        this.modelFile = modelFile;
        start();
    }

    /**
     * Writes to file if output is supplied, will print output to the commandline if no output file is supplied
     */
    private void start() {
        try {
            Logistic classifierModel = loadClassifier();
            Instances unknownInstances = loadArff(unknownFile);
            Instances labels = classifyNewInstance(classifierModel, unknownInstances);
            if (outputfile == null){
                System.out.println(labels);
            }
            else{
               writeArff(labels);
            }
        } catch (Exception ex) {
            throw new IllegalArgumentException("Your model file was not found, or you did not supply a proper model file. Use a model file using logistic regression or use the default model provided in the model folder, your model file was " + modelFile);
        }
    }

    /**
     * Finds the labels from the model, performs the logistic regression on the unknown instances then adds the label to the unknown dataset
     * @param log
     * @param unknownInstances
     * @return labeled Instances object
     * @throws Exception
     */
    private Instances classifyNewInstance(Logistic log, Instances unknownInstances) throws Exception {
        // create copy
        Instances labeled = new Instances(unknownInstances);
        // label instances
        for (int i = 0; i < unknownInstances.numInstances(); i++) {

            double clsLabel = log.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        return(labeled);
    }

    /**
     * Performs logistic regression
     * @return
     * @throws Exception
     */
    private Logistic loadClassifier() throws Exception {
        // deserialize model
        return (Logistic) weka.core.SerializationHelper.read(modelFile);
    }

    /**
     *
     * @param datafile
     * @return
     * @throws IOException
     * Loads instances from the unknown datafile and returns it as an Instances object
     */
    private Instances loadArff(String datafile) throws IOException {
        try {
            DataSource source = new DataSource(datafile);
            Instances data = source.getDataSet();
            // setting class attribute if the data format does not provide this information
            // For example, the XRFF format saves the class attribute information as well
            if (data.classIndex() == -1)
                data.setClassIndex(data.numAttributes() - 1);
            return data;
        } catch (Exception e) {
            throw new IOException("could not read from input file" + unknownFile);
        }
    }

    /**
     *
     * @param labels
     * @throws Exception
     * Writes WEKA output to .arff file if file does not exist.
     */
    private void writeArff(Instances labels) throws Exception{
        File f = new File(outputfile);
        if(f.exists()) {
            throw new IOException("File already exists, please remove the file before writing or enter a different output filename");
        }
        else{
            /**
             * Writes label output to output file
             */
            try(BufferedWriter writer = new BufferedWriter(new FileWriter(f))){
                writer.write(labels.toString());
            }
        }
    }
}
