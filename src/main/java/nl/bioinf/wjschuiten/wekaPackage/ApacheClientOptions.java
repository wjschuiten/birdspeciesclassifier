package nl.bioinf.wjschuiten.wekaPackage;

import org.apache.commons.cli.*;
import org.apache.commons.cli.CommandLineParser;
import weka.core.stopwords.Null;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ApacheClientOptions implements OptionProvider{
    private static final String INPUT_FILE = "input_file";
    private static final String OUTPUT_FILE = "output_file";
    private static final String MODEL_FILE = "model_file";
    private static final String HELP = "help";
    private final String[] clArguments;
    private Options options;
    private CommandLine commandLine;

    /**
     * @param args
     */

    public ApacheClientOptions(final String[] args){
            this.clArguments = args;
            buildFileOption();
            processCommandLine();

    }

    /**
     * Builds Options objects
     */
    private void buildFileOption() {
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "Prints the help screen");
        Option inputFileOption = new Option ("f", INPUT_FILE, true, "Filename to import from, must be a .arff file");
        Option outputFileOption = new Option ("o", OUTPUT_FILE, true, "Filename to export to");
        Option modelFileOption = new Option ("m", MODEL_FILE, true, "Model to take alghoritm from, use a logistic model file");
        options.addOption(inputFileOption);
        options.addOption(outputFileOption);
        options.addOption(helpOption);
        options.addOption(modelFileOption);
    }

    /**
     * Initiates help if user requests it
     * @return
     */
    public boolean helpRequested() { return this.commandLine.hasOption(HELP);}

    /**
     * Takes argument from commandline, stores them into object variables
     * Throws exception if commandline cannot be parsed
     */
    private void processCommandLine() {
            try {
                CommandLineParser parser = new DefaultParser();
                this.commandLine = parser.parse(this.options, this.clArguments);
            } catch (IllegalArgumentException | NullPointerException ex) {
                throw new IllegalArgumentException(ex);
            } catch(ParseException ex){
                throw new IllegalArgumentException("Your inputfile and/or outputfile had no input. If you do not wish to use an output file, only use -f");
            }
        }

    /**
     * validates if a .csv or .arff file was provided, throws exception if not
     * @param file
     */
    public static void validateFileName(String file){
            if(file == null){
                throw new IllegalArgumentException("You did not supply a filename");
        }
            Pattern p = Pattern.compile("([^\\s]+(\\.(?i)(arff))$)");
            Matcher m = p.matcher(file);
            if (!m.find()) {
                throw new IllegalArgumentException("No valid filename was supplied, or you did not use a .arff file for in and output. Your filename was: " + file);
            }
    }

    /**
     * @return input file argument of commandline
     */
    @Override
    public String getInputFileName() { return this.commandLine.getOptionValue(INPUT_FILE);}

    /**
     * @return output file argument of commandline
     */
    @Override
    public String getOutputFileName(){ return this.commandLine.getOptionValue(OUTPUT_FILE);}

    /**
     *
     * @return model file argument of commandline
     */
    @Override
    public String getModelFileName(){ return this.commandLine.getOptionValue(MODEL_FILE);}

    /**
     * Prints help to user
     */
    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("Help for the WEKA tool .jar", options);
    }
}
